package com.agileactors.service;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.agileactors.config.VaultConfiguration;

@Service
class PaymentService {
    private static Logger LOGGER = LoggerFactory.getLogger(PaymentService.class);

    private VaultConfiguration vaultConfiguration;

    @Autowired
    public PaymentService(VaultConfiguration vaultConfiguration) {
        this.vaultConfiguration = vaultConfiguration;
    }

    @PostConstruct
    void init() {
        LOGGER.info("PaymentService[username:{}]", vaultConfiguration.getUsername());
        LOGGER.info("PaymentService[password:{}]", vaultConfiguration.getPassword());
    }
}
